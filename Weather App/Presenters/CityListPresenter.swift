//
//  CityListPresenter.swift
//  Weather App
//
//  Created by scales on 12.01.2018.
//  Copyright © 2018 kpi. All rights reserved.
//

import PromiseKit

class CityListPresenter {

    private weak var cityListView: CityListView?
    private let openWeatherService = OpenWeatherService()
    private let citiesCDM = CitiesCoreDataManager.sharedInstance

    func attachView(view: CityListView) {
        cityListView = view
    }

    func detachCityListView() {
        cityListView = nil
    }

    func getCities(with ids: [String]) {
        for id in ids {
            loadWeather(for: id)
        }
    }
    
    func getFavouriteCities() {
        let cities = citiesCDM.getFavoriteCities()
        cities.forEach { cityListView?.addCity($0) }
    }
    
    func getCity(with id: String) {
        loadWeather(for: id)
    }

    private func loadWeather(for cityId: String) {
        guard let city = citiesCDM.getCityWith(id: cityId) else { return }
        firstly {
            openWeatherService.loadWeather(forCityId: cityId)
        }.done { weather in
            city.replaceCurrentWeather(with: weather)
            CoreDataManager.sharedInstance.saveContext()
        }.ensure { [weak self] in
            self?.cityListView?.addCity(city)
        }.catch { error in
            print(error.localizedDescription)
        }
    }
}

