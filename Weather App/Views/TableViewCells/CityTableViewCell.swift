//
//  CityTableViewCell.swift
//  Weather App
//
//  Created by scales on 10.01.2018.
//  Copyright © 2018 kpi. All rights reserved.
//

import UIKit
import Kingfisher

class CityTableViewCell: UITableViewCell {

    static let reuseID = "CityTableViewCell"
    
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var cityNameLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var tempLabel: UILabel!
    
    func configure(for city: City) {
        guard let weather = city.currentWeather else {
            detailLabel.text = "Error loading weather"
            cityNameLabel.text = "😢"
            return
        }
        self.cityNameLabel.text = city.name + ", " + city.country
        self.tempLabel.text = "\(getTempString(from: city.currentWeather?.temp))"
        self.detailLabel.text = """
        \(weather.descr.firstUppercased)
        Min temp \(getTempString(from: weather.minTemp)), max \(getTempString(from: weather.maxTemp))
        Wind is \(getWindDirectionString(from: weather.windDegree)), \(getWindSpeedString(from: weather.windSpeed))
        \(getHumidityString(from: weather.humidity).firstUppercased)
        \(getPressureString(from: weather.pressure).firstUppercased)
        """
        self.weatherImage.kf.setImage(with: city.currentWeather?.iconURL)
    }
    
}
