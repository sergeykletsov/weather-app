//
//  City+CoreDataClass.swift
//  Weather App
//
//  Created by scales on 23.02.2018.
//  Copyright © 2018 kpi. All rights reserved.
//
//

import Foundation
import CoreData


public class City: NSManagedObject {

    var currentWeather: Weather? {
        return (forecast?.array as? [Weather])?.first { $0.date == nil }
    }
    
    func replaceForecast(with forecast: [Weather]) {
        guard let currentWeather = currentWeather else { return }
        
        if let oldForecast = self.forecast {
            removeFromForecast(oldForecast)
        }
        
        let newArray = [currentWeather] + forecast
        let orderedSet = NSOrderedSet(array: newArray)
        addToForecast(orderedSet)
        forecastDate = NSDate()
    }
    
    func replaceCurrentWeather(with weather: Weather) {
        if let currentWeather = currentWeather {
            removeFromForecast(currentWeather)
        }
        addToForecast(weather)
        weatherDate = NSDate()
    }
    
}
