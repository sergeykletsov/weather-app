//
//  CitiesCoreDataManager.swift
//  Weather App
//
//  Created by scales on 17.02.2018.
//  Copyright © 2018 kpi. All rights reserved.
//

import Foundation
import PromiseKit
import CoreData

enum ErrorCitiesManager: String, Error {
    case noStringInData = "Cannot create string with given data"
    case alreadyUpToDate = "Cities in Core Data already up to date"
    
    var localizedDescription: String {
        return self.rawValue
    }
}

class CitiesCoreDataManager {
    private typealias StringsWithHash = (strings: [String], hash: Int)
    
    static let sharedInstance = CitiesCoreDataManager()
    private let userDefaults = UserDefaults.standard
    
    private init() {}
    
    func updateCitiesIfNeeded() {
        firstly {
            OpenWeatherService().loadCities()
        }.then(on: .global()) {
            self.getCitiesStringArray(from: $0)
        }.done {
            self.updateCitiesAndHash($0)
        }.catch { error in
            print(error)
        }
    }
    
    func getCityWith(id: String) -> City? {
        let request: NSFetchRequest<City> = City.fetchRequest()
        let predicate = NSPredicate(format: "id == %@", id)
        let context = CoreDataManager.sharedInstance.persistentContainer.viewContext
        
        request.predicate = predicate
        
        do {
            return try context.fetch(request).first
        } catch {
            print(error)
            return nil
        }
    }
    
    func getCitiesWith(name: String, offset: Int) -> [City] {
        let request: NSFetchRequest<City> = City.fetchRequest()
        let predicate = NSPredicate(format: "name contains[cd] %@", name)
        let context = CoreDataManager.sharedInstance.persistentContainer.viewContext
        
        request.predicate = predicate
        request.fetchLimit = 20
        request.fetchOffset = offset
        
        do {
            return try context.fetch(request)
        } catch {
            print(error)
            return []
        }
        
    }
    
    func getFavoriteCities() -> [City] {
        let request: NSFetchRequest<City> = City.fetchRequest()
        let predicate = NSPredicate(format: "isFaved == true")
        let context = CoreDataManager.sharedInstance.persistentContainer.viewContext
        
        request.predicate = predicate
        
        do {
            return try context.fetch(request)
        } catch {
            print(error)
            return []
        }
    }
    
    private func getCitiesStringArray(from data: Data) -> Promise<StringsWithHash> {
        return Promise<StringsWithHash> { promise in
            // check last hash
            let lastHash = userDefaults.value(forKey: "CitiesHash") as? Int
            if data.hashValue == lastHash {
                throw ErrorCitiesManager.alreadyUpToDate
            }
            
            guard let citiesString = String(data: data, encoding: .ascii) else {
                throw ErrorCitiesManager.noStringInData
            }
            
            let retultString = Array(citiesString.components(separatedBy: "\n").dropFirst().dropLast())
            let result: StringsWithHash = (retultString, data.hashValue)
            promise.fulfill(result)
        }
    }
    
    private func updateCitiesAndHash(_ stringsWithHash: StringsWithHash) {
        let container = CoreDataManager.sharedInstance.persistentContainer
        
        container.performBackgroundTask { [weak self] context in
            stringsWithHash.strings.forEach {
                let subCity = $0.components(separatedBy: "\t")
                if let id = subCity[safe: 0], let name = subCity[safe: 1], let lat = subCity[safe: 2], let lon = subCity[safe: 3], let country = subCity.last {
                    let city = City(context: context)
                    city.id = id
                    city.name = name
                    city.country = country
                    city.lat = Double(lat) ?? 0
                    city.lon = Double(lon) ?? 0
                }
            }
            
            do {
                try context.save()
            } catch {
                print("Failure to save context: \(error)")
            }
            
            self?.userDefaults.set(stringsWithHash.hash, forKey: "CitiesHash")
        }
        
    }
    
}
