//
//  OpenWeatherService.swift
//  Weather App
//
//  Created by scales on 09.01.2018.
//  Copyright © 2018 kpi. All rights reserved.
//

import UIKit
import PromiseKit

enum ErrorService: String, Error {
    case cityParsing = "Cannot create city"
    case forecastParsing = "Cannot create forecast"
    
    var localizedDescription: String {
        return self.rawValue
    }
}

class OpenWeatherService {
    
    private let client = HTTPClient(baseURL: Constants.Config.baseURL)
    
    func loadWeather(forCityId id: String, units: String? = nil, lang: String? = nil) -> Promise<Weather> {
        let path = getPath(for: WeatherType.weather.rawValue)
        let parameters = getParameters(forCityId: id, units: units, lang: lang)
        
        return firstly {
            client.startLoad(forPath: path, parameters: parameters)
            }.then { json -> Promise<Weather> in
                guard let city = Weather(json: json) else { throw ErrorService.cityParsing }
                return .value(city)
        }
    }

    func loadForecast(forCityId id: String, units: String? = nil, lang: String? = nil) -> Promise<[Weather]> {
        let path = getPath(for: WeatherType.forecast.rawValue)
        let parameters = getParameters(forCityId: id, units: units, lang: lang)
        
        return firstly {
            client.startLoad(forPath: path, parameters: parameters)
            }.then(on: .global()) { json -> Promise<[Weather]> in
                guard let weather = Weather.getForecast(from: json) else { throw ErrorService.forecastParsing }
                return .value(weather)
        }
    }
    
    func loadCities() -> Promise<Data> {
        let nonApiClient = HTTPClient(baseURL: Constants.Config.nonAPIBaseURL)
        let path = Constants.Config.citiesPath
        let fileName = Constants.Config.citiesFileName

        return nonApiClient.startLoadTXT(forPath: path, fileName: fileName)
    }
    
    private func getPath(for weatherType: String) -> String {
        return Constants.Config.basePath + weatherType
    }
    
    private func getParameters(forCityId cityId: String, units: String?, lang: String?) -> [String: Any] {
        return ["id": cityId,
                "appId": Constants.Config.appid,
                "lang": lang ?? Lang.default.rawValue,
                "units": units ?? Units.default.rawValue]
    }
    
}
